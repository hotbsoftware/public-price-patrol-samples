# Sample Calls to Price Patrol

Below includes a list of 3 sample calls to our search endpoints.

## Searching for "Halloween" without any custom params.

### Request:

```json
{
  "adid": "4c0f3c08-6903-408d-acb4-8aa14b48b1a3",
  "autocomplete": "1",
  "deviceType": "kinzie;android : 7.0",
  "geo_lat": 33.6321598,
  "geo_lon": -117.7348399,
  "keywords": "halloween",
  "location": "33.6321598,-117.7348399",
  "minprice": "0",
  "online": "0",
  "page_num": 1,
  "page_size": 20,
  "price": "0",
  "radius": "10",
  "requestorID": "9ea9976d8317735"
}
```

### Response:

```json
{
  "format": "json",
  "errorsExist": 0,
  "successMessage": "Item found",
  "results": {
    "retailers": [
      {
        "ID": "37207e12-12f5-4de2-a7a2-b70feb230df2",
        "Name": "Best Buy",
        "PromotionalContent": null,
        "Logo": "http:\/\/images.REMOVED_URL.com\/logos\/bestbuy.png",
        "LogoSquare": "http:\/\/images.REMOVED_URL.com\/logos\/bestbuy.png",
        "Phs": "1",
        "Url": "http:\/\/www.bestbuy.com\/"
      },
      {
        "ID": "c521d702-99e6-4417-955e-252ce945bd4d",
        "Name": "Torrid",
        "PromotionalContent": null,
        "Logo": "http:\/\/images.REMOVED_URL.com\/logos\/torrid-120x30.jpg",
        "LogoSquare": "http:\/\/images.REMOVED_URL.com\/logos\/torrid-50x50.jpg",
        "Url": "http:\/\/www.torrid.com\/"
      },
      {
        "ID": "1ef9a49b-366b-43b6-9c4c-68b1c4c2d402",
        "Name": "Target",
        "PromotionalContent": null,
        "Logo": "http:\/\/images.REMOVED_URL.com\/logos\/target.jpg",
        "LogoSquare": "http:\/\/images.REMOVED_URL.com\/logos\/target.jpg",
        "Phs": "8"
      },
      {
        "ID": "1aadb3df-0b06-40af-99a7-f8df6ed29333",
        "Name": "H&M",
        "PromotionalContent": null,
        "Logo": "http:\/\/images.REMOVED_URL.com\/logos\/handm.png",
        "LogoSquare": "http:\/\/images.REMOVED_URL.com\/logos\/handm.png"
      },
      {
        "ID": "0f05762f-95d9-4aa3-b954-01b67af6dd80",
        "Name": "Carter's",
        "PromotionalContent": null,
        "Logo": "http:\/\/images.REMOVED_URL.com\/logos\/carters.jpg",
        "LogoSquare": "http:\/\/images.REMOVED_URL.com\/logos\/carters.jpg",
        "Url": "http:\/\/www.carters.com\/"
      }
    ],
    "locations": [
      {
        "ID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Name": "Best Buy - Irvine",
        "RetailerID": "37207e12-12f5-4de2-a7a2-b70feb230df2",
        "Address": {
          "Line1": "16171 Lake Forest Dr",
          "City": "Irvine",
          "State": "CA",
          "ZIP": "92618",
          "Country": "US"
        },
        "Phone": "9497880840",
        "Latitude": "33.6305",
        "Longitude": "-117.72491",
        "MapLink": "http:\/\/maps.google.com\/maps?q=16171+Lake+Forest+Dr+Irvine+CA+92618",
        "Distance": "0.58",
        "PromotionalContent": null,
        "TNavLink": "http:\/\/apps.scout.me\/v1\/driveto?dt=16171+Lake+Forest+Dr+Irvine+CA+92618@33.6305,-117.72491&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Lake+Forest",
        "Hours": "1:11:00:20:00,2:10:00:21:00,3:10:00:21:00,4:10:00:21:00,5:10:00:21:00,6:10:00:21:00,7:10:00:21:00",
        "RetLocationId": "854",
        "TimeZone": "America\/Los_Angeles"
      },
      {
        "ID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Name": "Torrid - Irvine",
        "RetailerID": "c521d702-99e6-4417-955e-252ce945bd4d",
        "Address": {
          "Line1": "83 Fortune Drive",
          "City": "Irvine",
          "State": "CA",
          "ZIP": "92618",
          "Country": "US"
        },
        "Phone": "9495859380",
        "Latitude": "33.6489675",
        "Longitude": "-117.7449186",
        "MapLink": "http:\/\/maps.google.com\/maps?q=83+Fortune+Drive+Irvine+CA+92618",
        "Distance": "1.3",
        "PromotionalContent": null,
        "TNavLink": "http:\/\/apps.scout.me\/v1\/driveto?dt=83+Fortune+Drive+Irvine+CA+92618@33.6489675,-117.7449186&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Spectrum",
        "RetLocationId": "22",
        "TimeZone": "America\/Los_Angeles"
      },
      {
        "ID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Name": "Target - Irvine",
        "RetailerID": "1ef9a49b-366b-43b6-9c4c-68b1c4c2d402",
        "Address": {
          "Line1": "900 Spectrum Center Dr",
          "City": "Irvine",
          "State": "CA",
          "ZIP": "92618-4958",
          "Country": "US"
        },
        "Phone": "9498850114",
        "Latitude": "33.6490313",
        "Longitude": "-117.7449241",
        "MapLink": "http:\/\/maps.google.com\/maps?q=900+Spectrum+Center+Dr+Irvine+CA+92618-4958",
        "Distance": "1.3",
        "PromotionalContent": null,
        "TNavLink": "http:\/\/apps.scout.me\/v1\/driveto?dt=900+Spectrum+Center+Dr+Irvine+CA+92618-4958@33.6490313,-117.7449241&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Target",
        "Hours": "1:8:00:23:00,2:8:00:23:00,3:8:00:23:00,4:8:00:23:00,5:8:00:23:00,6:8:00:23:00,7:8:00:00:00",
        "RetLocationId": "2128",
        "TimeZone": "America\/Los_Angeles"
      },
      {
        "ID": "6163f926-4082-4904-99b9-5bdb5cb8c08a",
        "Name": "H&M - Irvine",
        "RetailerID": "1aadb3df-0b06-40af-99a7-f8df6ed29333",
        "Address": {
          "Line1": "711 Spectrum Center Dr",
          "City": "Irvine",
          "State": "CA",
          "ZIP": "92618",
          "Country": "US"
        },
        "Phone": "9497540681",
        "Latitude": "33.6490314",
        "Longitude": "-117.7449241",
        "MapLink": "http:\/\/maps.google.com\/maps?q=711+Spectrum+Center+Dr+Irvine+CA+92618",
        "Distance": "1.3",
        "PromotionalContent": null,
        "TNavLink": "http:\/\/apps.scout.me\/v1\/driveto?dt=711+Spectrum+Center+Dr+Irvine+CA+92618@33.6490314,-117.7449241&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=H+%26+M",
        "RetLocationId": "29",
        "TimeZone": "America\/Los_Angeles"
      },
      {
        "ID": "c951e768-3f66-4747-b952-26b15aa0c838",
        "Name": "Carter's - Irvine",
        "RetailerID": "0f05762f-95d9-4aa3-b954-01b67af6dd80",
        "Address": {
          "Line1": "105 Fortune Dr.",
          "City": "Irvine",
          "State": "CA",
          "ZIP": "92618",
          "Country": "US"
        },
        "Phone": "9497274124",
        "Latitude": "33.6490314",
        "Longitude": "-117.7449241",
        "MapLink": "http:\/\/maps.google.com\/maps?q=105+Fortune+Dr.+Irvine+CA+92618",
        "Distance": "1.3",
        "PromotionalContent": null,
        "TNavLink": "http:\/\/apps.scout.me\/v1\/driveto?dt=105+Fortune+Dr.+Irvine+CA+92618@33.6490314,-117.7449241&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Carter%27s+Spectrum+Center",
        "RetLocationId": "1044503480",
        "TimeZone": "America\/Los_Angeles"
      }
    ],
    "products": [
      {
        "@attributes": {
          "order": "1"
        },
        "ID": "22022021-ef25-4696-be75-f681924bba4e",
        "Name": "The Halloween [CD]",
        "MSRP": "13.99",
        "URL": "http:\/\/www.bestbuy.com\/site\/the-halloween-cd\/20761596.p?skuId=20761596&cmp=RMX",
        "ImageSmall": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/2076\/20761596_s.gif",
        "ImageLarge": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/2076\/20761596_sa.jpg",
        "LastUpdated": "2017-09-22T02:13:24:694+0000",
        "SKU": "20761596",
        "Model": "19466613",
        "Categories": {
          "Primary": "Compact Disc"
        },
        "ProductCategory": "Media",
        "QuantityText": "Expect availability",
        "Price": "13.99",
        "LocationID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Distance": 0.58,
        "ExternalProductId": "2617435",
        "RetLocationId": "854",
        "ReserveURL": "http:\/\/www.bestbuy.com\/site\/the-halloween-cd\/20761596.p?skuId=20761596&cmp=RMX",
        "Barcode": "00825279706824"
      },
      {
        "@attributes": {
          "order": "2"
        },
        "ID": "9bc4d68b-a356-42c0-a5eb-e6a06c14778b",
        "Name": "It's Halloween [CD]",
        "MSRP": "16.99",
        "URL": "http:\/\/www.bestbuy.com\/site\/its-halloween-cd\/21299448.p?skuId=21299448&cmp=RMX",
        "ImageSmall": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/2129\/21299448_s.gif",
        "ImageLarge": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/2129\/21299448_sa.jpg",
        "LastUpdated": "2017-09-22T02:13:24:694+0000",
        "SKU": "21299448",
        "Model": "20570040",
        "Categories": {
          "Primary": "Compact Disc"
        },
        "ProductCategory": "Media",
        "QuantityText": "Expect availability",
        "Price": "16.99",
        "LocationID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Distance": 0.58,
        "ExternalProductId": "2691725",
        "RetLocationId": "854",
        "ReserveURL": "http:\/\/www.bestbuy.com\/site\/its-halloween-cd\/21299448.p?skuId=21299448&cmp=RMX",
        "Barcode": "00617786000126"
      },
      {
        "@attributes": {
          "order": "3"
        },
        "ID": "4ec6d599-9ddd-4cb7-85e8-bff97759238a",
        "Name": "Halloween [CD]",
        "MSRP": "8.99",
        "URL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/5840888.p?skuId=5840888&cmp=RMX",
        "ImageSmall": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/5840\/5840888_s.gif",
        "ImageLarge": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/5840\/5840888_sa.jpg",
        "LastUpdated": "2017-09-22T02:13:24:694+0000",
        "SKU": "5840888",
        "Model": "1031",
        "Categories": {
          "Primary": "Compact Disc"
        },
        "ProductCategory": "Media",
        "QuantityText": "Expect availability",
        "Price": "8.99",
        "LocationID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Distance": 0.58,
        "ExternalProductId": "1323625",
        "RetLocationId": "854",
        "ReserveURL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/5840888.p?skuId=5840888&cmp=RMX",
        "Barcode": "00012805103121"
      },
      {
        "@attributes": {
          "order": "4"
        },
        "ID": "5238f37f-4ff0-44f1-873e-3a8cd399d589",
        "Name": "Halloween [CD]",
        "MSRP": "12.99",
        "URL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/8761645.p?skuId=8761645&cmp=RMX",
        "ImageSmall": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/8761\/8761645_s.gif",
        "ImageLarge": "https:\/\/img.bbystatic.com\/BestBuy_US\/images\/products\/8761\/8761645_sa.jpg",
        "LastUpdated": "2017-09-22T02:13:24:694+0000",
        "SKU": "8761645",
        "Model": "13395636",
        "Categories": {
          "Primary": "Compact Disc"
        },
        "ProductCategory": "Media",
        "QuantityText": "Expect availability",
        "Price": "12.99",
        "LocationID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Distance": 0.58,
        "ExternalProductId": "1825137",
        "RetLocationId": "854",
        "ReserveURL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/8761645.p?skuId=8761645&cmp=RMX",
        "Barcode": "00678277170926"
      },
      {
        "@attributes": {
          "order": "5"
        },
        "ID": "872c02cd-80c7-44b7-ac1f-820acbf04682",
        "Name": "Halloween [CD]",
        "MSRP": "5.99",
        "URL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/11517325.p?skuId=11517325&cmp=RMX",
        "LastUpdated": "2017-09-22T02:13:24:694+0000",
        "SKU": "11517325",
        "Model": "00000000000610648",
        "Categories": {
          "Primary": "Compact Disc"
        },
        "ProductCategory": "Media",
        "QuantityText": "Expect availability",
        "Price": "5.99",
        "LocationID": "f34f4ff1-7c26-4a6d-9dce-6636eaa00f88",
        "Distance": 0.58,
        "ExternalProductId": "3175752",
        "RetLocationId": "854",
        "ReserveURL": "http:\/\/www.bestbuy.com\/site\/halloween-cd\/11517325.p?skuId=11517325&cmp=RMX",
        "Barcode": "00090204996711"
      },
      {
        "@attributes": {
          "order": "6"
        },
        "ID": "894fa9ea-ccbf-4d32-9aae-ff077fe13deb",
        "Name": "Halloween Corset Tulle Dress",
        "URL": "http:\/\/www.gopjn.com\/t\/1-6258-65790-6258?url=http%3A%2F%2Flink.mercent.com%2Fredirect.ashx%3Fmr%3AmerchantID%3DTorrid%26mr%3AtrackingCode%3D015B4017-4D57-E511-80F8-0050569475F3%26mr%3AtargetUrl%3Dhttp%3A%2F%2Fwww.torrid.com%2Fproduct%2Fhalloween-corset-tulle-dress%2F10419720.html%253fcm_mmc%253dAFF-_-PJM-_-%7Bsubid%7D-_-5_9999W1_AFF_PJM_%7Bsubid%7D_FEED%2526source%253dPJ_AD%3AZ%3ATOR%2526affiliateId%253d%7Bsubid%7D%2526clickId%253d%7Bclid%7D%2526affiliateCustomId%253d%7Bsid%7D",
        "ImageSmall": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=100",
        "ImageLarge": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=1200",
        "LastUpdated": "2016-07-01T09:49:21:412+0000",
        "DescriptionShort": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "DescriptionLong": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "SKU": "10419720",
        "Categories": {
          "Primary": "Halloween",
          "Secondary": "Halloween Collection"
        },
        "Size": "20",
        "Color": "BLACK",
        "QuantityText": "Expect availability",
        "Price": "37.98",
        "LocationID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Distance": 1.3,
        "ExternalProductId": "3a5b9f3e.17021d49_10419720",
        "RetLocationId": "22"
      },
      {
        "@attributes": {
          "order": "7"
        },
        "ID": "bfe69857-27d4-463d-a8db-297adb7b2cc2",
        "Name": "Halloween Tails Jacket",
        "URL": "http:\/\/www.pntrac.com\/t\/1-6258-65790-6258?url=http%3A%2F%2Flink.mercent.com%2Fredirect.ashx%3Fmr%3AmerchantID%3DTorrid%26mr%3AtrackingCode%3D125B4017-4D57-E511-80F8-0050569475F3%26mr%3AtargetUrl%3Dhttp%3A%2F%2Fwww.torrid.com%2Fproduct%2Fhalloween-tails-jacket%2F10420097.html%253fcm_mmc%253dAFF-_-PJM-_-%7Bsubid%7D-_-5_9999W1_AFF_PJM_%7Bsubid%7D_FEED%2526source%253dPJ_AD%3AZ%3ATOR%2526affiliateId%253d%7Bsubid%7D%2526clickId%253d%7Bclid%7D%2526affiliateCustomId%253d%7Bsid%7D",
        "ImageSmall": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10420093_hi?hei=100",
        "ImageLarge": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10420093_hi?hei=1200",
        "LastUpdated": "2016-07-01T09:49:21:412+0000",
        "DescriptionShort": "We borrowed classic tuxedo tails from the boys and made it our own for Halloween. The black jacket has a simple blazer-inspired front, while the back lace up detail and cascading back tails help you make quite the entrance. Model is 5'9\", size 1",
        "DescriptionLong": "We borrowed classic tuxedo tails from the boys and made it our own for Halloween. The black jacket has a simple blazer-inspired front, while the back lace up detail and cascading back tails help you make quite the entrance. Model is 5'9\", size 1",
        "SKU": "10420097",
        "Categories": {
          "Primary": "Halloween",
          "Secondary": "Halloween Collection"
        },
        "Size": "3",
        "Color": "BLACK",
        "QuantityText": "Expect availability",
        "Price": "68.90",
        "LocationID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Distance": 1.3,
        "ExternalProductId": "b95ea5c2.d7bb086f_10420097",
        "RetLocationId": "22",
        "Barcode": "00010420097108"
      },
      {
        "@attributes": {
          "order": "8"
        },
        "ID": "5ddd099d-8fb0-47b8-8430-f81552f94a8a",
        "Name": "Halloween Corset Tulle Dress",
        "URL": "http:\/\/www.pjatr.com\/t\/1-6258-65790-6258?url=http%3A%2F%2Flink.mercent.com%2Fredirect.ashx%3Fmr%3AmerchantID%3DTorrid%26mr%3AtrackingCode%3DFD5A4017-4D57-E511-80F8-0050569475F3%26mr%3AtargetUrl%3Dhttp%3A%2F%2Fwww.torrid.com%2Fproduct%2Fhalloween-corset-tulle-dress%2F10419716.html%253fcm_mmc%253dAFF-_-PJM-_-%7Bsubid%7D-_-5_9999W1_AFF_PJM_%7Bsubid%7D_FEED%2526source%253dPJ_AD%3AZ%3ATOR%2526affiliateId%253d%7Bsubid%7D%2526clickId%253d%7Bclid%7D%2526affiliateCustomId%253d%7Bsid%7D",
        "ImageSmall": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=100",
        "ImageLarge": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=1200",
        "LastUpdated": "2016-07-01T09:49:21:412+0000",
        "DescriptionShort": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "DescriptionLong": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "SKU": "10419716",
        "Categories": {
          "Primary": "Halloween",
          "Secondary": "Halloween Collection"
        },
        "Size": "12",
        "Color": "BLACK",
        "QuantityText": "Expect availability",
        "Price": "37.98",
        "LocationID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Distance": 1.3,
        "ExternalProductId": "3a5b9f3e.17021d49_10419716",
        "RetLocationId": "22"
      },
      {
        "@attributes": {
          "order": "9"
        },
        "ID": "1b69a918-c636-4bf0-bb54-12bc0a07f755",
        "Name": "Halloween Corset Tulle Dress",
        "URL": "http:\/\/www.pntrs.com\/t\/1-6258-65790-6258?url=http%3A%2F%2Flink.mercent.com%2Fredirect.ashx%3Fmr%3AmerchantID%3DTorrid%26mr%3AtrackingCode%3D035B4017-4D57-E511-80F8-0050569475F3%26mr%3AtargetUrl%3Dhttp%3A%2F%2Fwww.torrid.com%2Fproduct%2Fhalloween-corset-tulle-dress%2F10419722.html%253fcm_mmc%253dAFF-_-PJM-_-%7Bsubid%7D-_-5_9999W1_AFF_PJM_%7Bsubid%7D_FEED%2526source%253dPJ_AD%3AZ%3ATOR%2526affiliateId%253d%7Bsubid%7D%2526clickId%253d%7Bclid%7D%2526affiliateCustomId%253d%7Bsid%7D",
        "ImageSmall": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=100",
        "ImageLarge": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=1200",
        "LastUpdated": "2016-07-01T09:49:21:412+0000",
        "DescriptionShort": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "DescriptionLong": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "SKU": "10419722",
        "Categories": {
          "Primary": "Halloween",
          "Secondary": "Halloween Collection"
        },
        "Size": "24",
        "Color": "BLACK",
        "QuantityText": "Expect availability",
        "Price": "37.98",
        "LocationID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Distance": 1.3,
        "ExternalProductId": "3a5b9f3e.17021d49_10419722",
        "RetLocationId": "22"
      },
      {
        "@attributes": {
          "order": "10"
        },
        "ID": "c93299c1-4af1-40fb-8288-fc98650483a3",
        "Name": "Halloween Corset Tulle Dress",
        "URL": "http:\/\/www.pjtra.com\/t\/1-6258-65790-6258?url=http%3A%2F%2Flink.mercent.com%2Fredirect.ashx%3Fmr%3AmerchantID%3DTorrid%26mr%3AtrackingCode%3DFE5A4017-4D57-E511-80F8-0050569475F3%26mr%3AtargetUrl%3Dhttp%3A%2F%2Fwww.torrid.com%2Fproduct%2Fhalloween-corset-tulle-dress%2F10419717.html%253fcm_mmc%253dAFF-_-PJM-_-%7Bsubid%7D-_-5_9999W1_AFF_PJM_%7Bsubid%7D_FEED%2526source%253dPJ_AD%3AZ%3ATOR%2526affiliateId%253d%7Bsubid%7D%2526clickId%253d%7Bclid%7D%2526affiliateCustomId%253d%7Bsid%7D",
        "ImageSmall": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=100",
        "ImageLarge": "http:\/\/hottopic.scene7.com\/is\/image\/HotTopic\/10419715_hi?hei=1200",
        "LastUpdated": "2016-07-01T09:49:21:412+0000",
        "DescriptionShort": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "DescriptionLong": "Bold and beguiling, this black corset dress is casting quite the spell. All set for a night of mischief with faux leather panels along the bust, a lace up front and lace trim along the sweetheart neckline. The tulle underlay gives an extra kick to the fitted style. Model is 5'9\", size 1",
        "SKU": "10419717",
        "Categories": {
          "Primary": "Halloween",
          "Secondary": "Halloween Collection"
        },
        "Size": "14",
        "Color": "BLACK",
        "QuantityText": "Expect availability",
        "Price": "37.98",
        "LocationID": "8604f00b-346f-446e-a3f4-6a4a5da7dde8",
        "Distance": 1.3,
        "ExternalProductId": "3a5b9f3e.17021d49_10419717",
        "RetLocationId": "22"
      },
      {
        "@attributes": {
          "order": "11"
        },
        "ID": "53b9a425-2821-469c-8748-d7280d276d74",
        "Name": "Halloween Pumpkin Tinsel Banner",
        "URL": "http:\/\/api.REMOVED_URL.com\/v2.0\/rdr?id=p:53b9a425-2821-469c-8748-d7280d276d74&requestId=1908b101-5288-43e8-aa83-98e52f143ebf&apikey=oFxl-L1CDxmKnlT18GloDNeo6sFzCwL8",
        "ImageSmall": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17425759?wid=60&hei=60",
        "ImageLarge": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17425759?wid=480&hei=480",
        "LastUpdated": "2016-03-31T15:31:11:740+0000",
        "DescriptionShort": "Halloween Pumpkin Tinsel Banner",
        "DescriptionLong": "Halloween Pumpkin Tinsel Banner",
        "SKU": "240-43-0512",
        "Categories": null,
        "QuantityText": "Expect availability",
        "Price": "4.00",
        "LocationID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Distance": 1.3,
        "ExternalProductId": "209032783",
        "RetLocationId": "2128"
      },
      {
        "@attributes": {
          "order": "12"
        },
        "ID": "6f1105be-b8b6-4809-b2c9-f624b25ad443",
        "Name": "Halloween Fabric Figural - Small",
        "ImageSmall": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17361080?wid=60&hei=60",
        "ImageLarge": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17361080?wid=480&hei=480",
        "LastUpdated": "2016-03-31T15:31:11:740+0000",
        "DescriptionShort": "Halloween Fabric Figural - Small",
        "DescriptionLong": "Halloween Fabric Figural - Small",
        "SKU": "240-43-0403",
        "Categories": null,
        "QuantityText": "Expect availability",
        "Price": "3.00",
        "LocationID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Distance": 1.3,
        "ExternalProductId": "209032745",
        "RetLocationId": "2128"
      },
      {
        "@attributes": {
          "order": "13"
        },
        "ID": "53034987-ad05-40f9-94cf-2bcc640abb5d",
        "Name": "Halloween Tinsel Banner - Green",
        "URL": "http:\/\/www.target.com\/p\/halloween-tinsel-banner-green\/-\/A-17443662",
        "ImageSmall": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17443662?wid=60&hei=60",
        "ImageLarge": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17443662?wid=480&hei=480",
        "LastUpdated": "2016-03-31T15:31:11:740+0000",
        "DescriptionShort": "Halloween Tinsel Banner - Green",
        "DescriptionLong": "Halloween Tinsel Banner - Green",
        "SKU": "240-43-0776",
        "Categories": null,
        "QuantityText": "Expect availability",
        "Price": "4.00",
        "LocationID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Distance": 1.3,
        "ExternalProductId": "209032489",
        "RetLocationId": "2128"
      },
      {
        "@attributes": {
          "order": "14"
        },
        "ID": "17be0c0a-46eb-404b-b8fd-43ed3c2de67d",
        "Name": "Halloween Balloons - 16ct",
        "URL": "http:\/\/www.target.com\/p\/halloween-balloons-16ct\/-\/A-17425761",
        "ImageSmall": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17425761?wid=60&hei=60",
        "ImageLarge": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17425761?wid=480&hei=480",
        "LastUpdated": "2016-03-31T15:31:11:740+0000",
        "DescriptionShort": "Halloween Balloons - 16ct",
        "DescriptionLong": "Halloween Balloons - 16ct",
        "SKU": "240-43-0478",
        "Categories": null,
        "QuantityText": "Expect availability",
        "Price": "1.50",
        "LocationID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Distance": 1.3,
        "ExternalProductId": "209032385",
        "RetLocationId": "2128"
      },
      {
        "@attributes": {
          "order": "15"
        },
        "ID": "6aada9e9-abd6-4dc2-8952-cc5435901b46",
        "Name": "Halloween Short Metal Bowl",
        "URL": "http:\/\/www.target.com\/p\/halloween-short-metal-bowl\/-\/A-17226098",
        "ImageSmall": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17226098?wid=60&hei=60",
        "ImageLarge": "http:\/\/scene7.targetimg1.com\/is\/image\/Target\/17226098?wid=480&hei=480",
        "LastUpdated": "2016-03-31T15:31:11:740+0000",
        "DescriptionShort": "Double, double toil and trouble&mdash;the Halloween Short Metal Serving Bowl instantly conjures the party spirit. Spooky black talons hold the bowl in their clutches, forming a dramatic base. A modern design-inspired serving bowl is ideal for chips and dip, fun-size candies, or any other spooky treats. Skip the streamers and balloons, this Halloween serving bowl does the hard work for you. It's an instant Halloween party decoration!",
        "DescriptionLong": "Double, double toil and trouble&mdash;the Halloween Short Metal Serving Bowl instantly conjures the party spirit. Spooky black talons hold the bowl in their clutches, forming a dramatic base. A modern design-inspired serving bowl is ideal for chips and dip, fun-size candies, or any other spooky treats. Skip the streamers and balloons, this Halloween serving bowl does the hard work for you. It's an instant Halloween party decoration!",
        "SKU": "200-06-2160",
        "Categories": {
          "Primary": "Dining & Entertaining",
          "Secondary": "Serveware"
        },
        "ProductCategory": "Home & Garden",
        "QuantityText": "Expect availability",
        "Price": "9.99",
        "LocationID": "9adb1a89-db73-4f5b-9a87-40d2fe3d9fc8",
        "Distance": 1.3,
        "ExternalProductId": "208735851",
        "RetLocationId": "2128"
      },
      {
        "@attributes": {
          "order": "16"
        },
        "ID": "08918f00-76d6-4026-a325-f0cb383cc82d",
        "Name": "Halloween Set",
        "URL": "http:\/\/www.hm.com\/us\/product\/27900?article=27900-A",
        "ImageSmall": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/facebook]",
        "ImageLarge": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/large]",
        "LastUpdated": "2015-03-18T20:12:43:763+0000",
        "DescriptionShort": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "DescriptionLong": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "SKU": "27900-A-09",
        "Categories": {
          "Primary": "Kids",
          "Secondary": "Baby Boy Size 4-24m",
          "Tertiary": "Sets & Overalls"
        },
        "Size": "1\u00c2\u00bd-2Y",
        "Color": "Orange",
        "QuantityText": "Expect availability",
        "Price": "19.95",
        "LocationID": "6163f926-4082-4904-99b9-5bdb5cb8c08a",
        "Distance": 1.3,
        "ExternalProductId": "27900_27900-a-09",
        "RetLocationId": "29"
      },
      {
        "@attributes": {
          "order": "17"
        },
        "ID": "d92264d8-7b99-408b-8e0b-a780ad17fbe6",
        "Name": "Halloween Set",
        "URL": "http:\/\/www.hm.com\/us\/product\/27900?article=27900-A",
        "ImageSmall": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/facebook]",
        "ImageLarge": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/large]",
        "LastUpdated": "2015-03-18T20:12:43:763+0000",
        "DescriptionShort": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "DescriptionLong": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "SKU": "27900-A-08",
        "Categories": {
          "Primary": "Kids",
          "Secondary": "Baby Boy Size 4-24m",
          "Tertiary": "Sets & Overalls"
        },
        "Size": "12-18M",
        "Color": "Orange",
        "QuantityText": "Expect availability",
        "Price": "19.95",
        "LocationID": "6163f926-4082-4904-99b9-5bdb5cb8c08a",
        "Distance": 1.3,
        "ExternalProductId": "27900_27900-a-08",
        "RetLocationId": "29"
      },
      {
        "@attributes": {
          "order": "18"
        },
        "ID": "a9f8dbec-f56f-4176-8f1f-62e3e693b1d8",
        "Name": "Halloween Set",
        "URL": "http:\/\/www.hm.com\/us\/product\/27900?article=27900-A",
        "ImageSmall": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/facebook]",
        "ImageLarge": "http:\/\/lp.hm.com\/hmprod?set=source[\/model\/2014\/S00%200231012%20001%2000%200000.jpg],rotate[],width[],height[],x[],y[],type[STILL_LIFE_FRONT]&hmver=0&call=url[file:\/product\/large]",
        "LastUpdated": "2015-03-18T20:12:43:763+0000",
        "DescriptionShort": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "DescriptionLong": "Pumpkin sweatshirt with matching hat in jersey. Snap fasteners at back of neck.",
        "SKU": "27900-A-05",
        "Categories": {
          "Primary": "Kids",
          "Secondary": "Baby Boy Size 4-24m",
          "Tertiary": "Sets & Overalls"
        },
        "Size": "4-6M",
        "Color": "Orange",
        "QuantityText": "Expect availability",
        "Price": "19.95",
        "LocationID": "6163f926-4082-4904-99b9-5bdb5cb8c08a",
        "Distance": 1.3,
        "ExternalProductId": "27900_27900-a-05",
        "RetLocationId": "29"
      },
      {
        "@attributes": {
          "order": "19"
        },
        "ID": "ff895adc-7d65-4a52-b0e4-ca228a0b4b71",
        "Name": "Little Firefighter Halloween Costume",
        "MSRP": "19.99",
        "URL": "http:\/\/api.shopstyle.com\/action\/apiVisitRetailer?id=530763517&pid=uid7041-26963929-59",
        "ImageSmall": "https:\/\/img.shopstyle-cdn.com\/sim\/71\/7f\/717f511ef432e615e30d5ead8eeccc8e_medium\/carters-little-firefighter-halloween-costume.jpg",
        "ImageLarge": "https:\/\/img.shopstyle-cdn.com\/mim\/71\/7f\/717f511ef432e615e30d5ead8eeccc8e.jpg",
        "LastUpdated": "2017-02-13T13:16:38:423+0000",
        "DescriptionShort": "The perfect Halloween costume for your little hero! Complete with a cozy firefighter hat hood, this costume is ready to trick-or-treat.     *   2-piece costume     *   Zip-front design     *   Jersey lining     *   No-pinch elastic waistband",
        "DescriptionLong": "The perfect Halloween costume for your little hero! Complete with a cozy firefighter hat hood, this costume is ready to trick-or-treat.     *   2-piece costume     *   Zip-front design     *   Jersey lining     *   No-pinch elastic waistband",
        "Categories": {
          "Primary": "Kids' Nursery",
          "Secondary": "Clothes And Toys",
          "Tertiary": "Boys' Clothing"
        },
        "ProductCategory": "Baby & Toddler",
        "Size": "3-6M",
        "QuantityText": "Expect availability",
        "Price": "19.99",
        "LocationID": "c951e768-3f66-4747-b952-26b15aa0c838",
        "Distance": 1.3,
        "ExternalProductId": "530763517",
        "RetLocationId": "1044503480"
      },
      {
        "@attributes": {
          "order": "20"
        },
        "ID": "7258e7f9-e4ef-43bc-b4cd-ee1545ce38ad",
        "Name": "Long-Sleeve Halloween Tee",
        "MSRP": "3.99",
        "URL": "http:\/\/api.shopstyle.com\/action\/apiVisitRetailer?id=531102977&pid=uid7041-26963929-59",
        "ImageSmall": "https:\/\/img.shopstyle-cdn.com\/sim\/93\/92\/939206df213deb8565f605cdd0a98478_medium\/carters-long-sleeve-halloween-tee.jpg",
        "ImageLarge": "https:\/\/img.shopstyle-cdn.com\/mim\/93\/92\/939206df213deb8565f605cdd0a98478.jpg",
        "LastUpdated": "2017-02-13T13:16:38:423+0000",
        "DescriptionShort": "Soft cotton jersey featuring spooky graphics perfect for Halloween. No tricks here, this tee is a treat!     *   Long sleeves     *   Ribbed neckline     *   Screen-printed Halloween necklace",
        "DescriptionLong": "Soft cotton jersey featuring spooky graphics perfect for Halloween. No tricks here, this tee is a treat!     *   Long sleeves     *   Ribbed neckline     *   Screen-printed Halloween necklace",
        "Categories": {
          "Primary": "Kids' Nursery",
          "Secondary": "Clothes And Toys",
          "Tertiary": "Girls' Tees"
        },
        "ProductCategory": "Baby & Toddler",
        "Size": "3M",
        "QuantityText": "Expect availability",
        "Price": "3.99",
        "LocationID": "c951e768-3f66-4747-b952-26b15aa0c838",
        "Distance": 1.3,
        "ExternalProductId": "531102977",
        "RetLocationId": "1044503480"
      }
    ]
  }
}
```


## Search for "iphone case" with a radius of 100 miles

### Request:

```json
{
  "adid": "4c0f3c08-6903-408d-acb4-8aa14b48b1a3",
  "category": "0",
  "deviceType": "kinzie;android : 7.0",
  "geo_lat": 33.6321625,
  "geo_lon": -117.7348413,
  "keywords": "iphone case",
  "location": "33.6321625,-117.7348413",
  "minprice": "0",
  "online": "0",
  "page_num": 1,
  "page_size": 20,
  "price": "0",
  "radius": "100",
  "requestorID": "9ea9976d8317735"
}
```

### Response:

```json
{
    "format": "json",
    "errorsExist": 0,
    "successMessage": "Item found",
    "results": {
        "retailers": [
            {
                "ID": "b8db044e-a155-474c-bf6b-01fe4f7bb070",
                "Name": "Golfsmith",
                "PromotionalContent": null,
                "Logo": "http://images.REMOVED_URL.com/logos/golfsmith.jpg",
                "LogoSquare": "http://images.REMOVED_URL.com/logos/golfsmith.jpg",
                "Phs": "1",
                "Url": "http://www.golfsmith.com/"
            },
            {
                "ID": "39bf339b-3c53-461c-a54a-e038b558b7d1",
                "Name": "Anthropologie",
                "PromotionalContent": null,
                "Logo": "http://images.REMOVED_URL.com/logos/anthropology.svg",
                "LogoSquare": "http://images.REMOVED_URL.com/logos/anthropology.svg",
                "Url": "http://www.anthropologie.com/"
            },
            {
                "ID": "5e763752-7cdd-4ed9-9152-b5d87aa87c23",
                "Name": "Nordstrom",
                "PromotionalContent": null,
                "Logo": "http://images.REMOVED_URL.com/logos/nordstrom.jpg",
                "LogoSquare": "http://images.REMOVED_URL.com/logos/nordstrom.jpg",
                "Phs": "1",
                "Url": "http://shop.nordstrom.com/"
            },
            {
                "ID": "67dc89e9-9dc9-4faa-9855-b8ca2f721d33",
                "Name": "Apple",
                "PromotionalContent": null,
                "Logo": "http://images.REMOVED_URL.com/logos/apple.jpg",
                "LogoSquare": "http://images.REMOVED_URL.com/logos/apple.jpg",
                "Url": "http://www.apple.com"
            },
            {
                "ID": "874dc833-1027-4233-89ac-84a1c7a1edb9",
                "Name": "Charlotte Russe",
                "PromotionalContent": null,
                "Logo": "http://images.REMOVED_URL.com/logos/charlotterusse.png",
                "LogoSquare": "http://images.REMOVED_URL.com/logos/charlotterusse.png",
                "Url": "http://www.charlotterusse.com/"
            },
            {
                "ID": "1f68dc4b-827c-4afd-8ae6-3b94d0f7e971",
                "Name": "francesca's",
                "PromotionalContent": null
            }
        ],
        "locations": [
            {
                "ID": "1b60750f-bdd9-4a74-9eec-719e158ae914",
                "Name": "Golfsmith - Irvine",
                "RetailerID": "b8db044e-a155-474c-bf6b-01fe4f7bb070",
                "Address": {
                    "Line1": "16181 Lake Forest Dr.",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618-4301",
                    "Country": "US"
                },
                "Phone": "9497274170",
                "Latitude": "33.6305071",
                "Longitude": "-117.72563",
                "MapLink": "http://maps.google.com/maps?q=16181+Lake+Forest+Dr.+Irvine+CA+92618-4301",
                "Distance": "0.54",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=16181+Lake+Forest+Dr.+Irvine+CA+92618-4301@33.6305071,-117.72563&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=LA+Irvine",
                "Hours": "1:10:00:18:00,2:9:00:21:00,3:9:00:21:00,4:9:00:21:00,5:9:00:21:00,6:9:00:21:00,7:9:00:19:00",
                "RetLocationId": "2284587883",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "8c14428a-6382-4e8c-a88c-65c3bbf1b1e7",
                "Name": "Anthropologie - Irvine",
                "RetailerID": "39bf339b-3c53-461c-a54a-e038b558b7d1",
                "Address": {
                    "Line1": "99 Fortune Dr",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9493410104",
                "Latitude": "33.6479089",
                "Longitude": "-117.7398563",
                "MapLink": "http://maps.google.com/maps?q=99+Fortune+Dr+Irvine+CA+92618",
                "Distance": "1.13",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=99+Fortune+Dr+Irvine+CA+92618@33.6479089,-117.7398563&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Spectrum+Center",
                "Hours": "1:10:00:20:00,2:10:00:21:00,3:10:00:21:00,4:10:00:21:00,5:10:00:21:00,6:10:00:22:00,7:10:00:22:00",
                "RetLocationId": "19",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Name": "Nordstrom - Irvine",
                "RetailerID": "5e763752-7cdd-4ed9-9152-b5d87aa87c23",
                "Address": {
                    "Line1": "800 Spectrum Center Drive",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9492552800",
                "Latitude": "33.6484984",
                "Longitude": "-117.7427271",
                "MapLink": "http://maps.google.com/maps?q=800+Spectrum+Center+Drive+Irvine+CA+92618",
                "Distance": "1.22",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=800+Spectrum+Center+Drive+Irvine+CA+92618@33.6484984,-117.7427271&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Nordstrom+Irvine+Spectrum+Center",
                "Hours": "1:11:00:20:00,2:10:00:21:00,3:10:00:21:00,4:10:00:21:00,5:10:00:21:00,6:10:00:21:30,7:10:00:21:30",
                "RetLocationId": "1289421894",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Name": "Apple - Irvine",
                "RetailerID": "67dc89e9-9dc9-4faa-9855-b8ca2f721d33",
                "Address": {
                    "Line1": "85 Fortune Drive",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9492551500",
                "Latitude": "33.6493714",
                "Longitude": "-117.7434194",
                "MapLink": "http://maps.google.com/maps?q=85+Fortune+Drive+Irvine+CA+92618",
                "Distance": "1.29",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=85+Fortune+Drive+Irvine+CA+92618@33.6493714,-117.7434194&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Spectrum+Center",
                "Hours": "1:10:00:20:00,2:10:00:21:00,3:10:00:21:00,4:10:00:21:00,5:10:00:21:00,6:10:00:22:00,7:10:00:22:00",
                "RetLocationId": "r146",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "5ee0d386-660f-4779-b404-6ae186e28e93",
                "Name": "Charlotte Russe - Irvine",
                "RetailerID": "874dc833-1027-4233-89ac-84a1c7a1edb9",
                "Address": {
                    "Line1": "358 Spectrum Center Drive",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9497271417",
                "Latitude": "33.6490314",
                "Longitude": "-117.7449241",
                "MapLink": "http://maps.google.com/maps?q=358+Spectrum+Center+Drive+Irvine+CA+92618",
                "Distance": "1.3",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=358+Spectrum+Center+Drive+Irvine+CA+92618@33.6490314,-117.7449241&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Spectrum",
                "Hours": "1:10:00:20:00,2:10:00:21:00,3:10:00:21:00,4:10:00:21:00,5:10:00:21:00,6:10:00:22:00,7:10:00:22:00",
                "RetLocationId": "2633180919",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "4e10da9d-0693-421e-b171-c22c273b4c2e",
                "Name": "francesca's - Irvine",
                "RetailerID": "1f68dc4b-827c-4afd-8ae6-3b94d0f7e971",
                "Address": {
                    "Line1": "520 Spectrum Center Drive",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9497531049",
                "Latitude": "33.6490314",
                "Longitude": "-117.7449241",
                "MapLink": "http://maps.google.com/maps?q=520+Spectrum+Center+Drive+Irvine+CA+92618",
                "Distance": "1.3",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=520+Spectrum+Center+Drive+Irvine+CA+92618@33.6490314,-117.7449241&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Irvine+Spectrum",
                "RetLocationId": "47",
                "TimeZone": "America/Los_Angeles"
            }
        ],
        "products": [
            {
                "@attributes": {
                    "order": "1"
                },
                "ID": "00ce260d-9c2a-4028-9a36-8cdadc6d6ba3",
                "Name": "Clicgear iPhone Adaptor Case",
                "URL": "http://www.tkqlhce.com/click-5017586-10304326?url=http%3A%2F%2Fwww.golfsmith.com%2Fproduct%2F30097347%2Fclicgear-iphone-adaptor-case%3Ftcode%3Dcj",
                "ImageSmall": "http://www.golfsmith.com/images/comingsoon_sm.jpg",
                "ImageLarge": "http://www.golfsmith.com/images/comingsoon_sm.jpg",
                "LastUpdated": "2016-06-01T10:26:34:721+0000",
                "DescriptionShort": "Pro Active Sports iPhone Adaptor Case Snap your iPhone into the Pro Active Sports iPhone Adaptor Case to allow the phone to work with Clicgear and View GPS/Phone Holders. Please note that the iPhone Adaptor Case will not fit over the top of any existing phone case.",
                "DescriptionLong": "Pro Active Sports iPhone Adaptor Case Snap your iPhone into the Pro Active Sports iPhone Adaptor Case to allow the phone to work with Clicgear and View GPS/Phone Holders. Please note that the iPhone Adaptor Case will not fit over the top of any existing phone case.",
                "SKU": "30097347",
                "Categories": {
                    "Primary": "Golf Cart Accessories"
                },
                "ProductCategory": "Sporting Goods",
                "QuantityText": "Expect availability",
                "Price": "3.48",
                "LocationID": "1b60750f-bdd9-4a74-9eec-719e158ae914",
                "Distance": 0.54,
                "ExternalProductId": "30097347",
                "RetLocationId": "2284587883",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=00ce260d-9c2a-4028-9a36-8cdadc6d6ba3&locationId=1b60750f-bdd9-4a74-9eec-719e158ae914&format=XML",
                "ReserveURL": "http://www.tkqlhce.com/click-5017586-10304326?url=http%3A%2F%2Fwww.golfsmith.com%2Fproduct%2F30097347%2Fclicgear-iphone-adaptor-case%3Ftcode%3Dcj"
            },
            {
                "@attributes": {
                    "order": "2"
                },
                "ID": "7b6048e5-f610-476d-950e-b7cad62668a7",
                "Name": "Molly Hatch iPhone 7 Case",
                "MSRP": "36.00",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=612180512&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/a1/53/a153cd6e77fbaec471ddf50da32a6c32_medium/molly-hatch-iphone-7-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/a1/53/a153cd6e77fbaec471ddf50da32a6c32.jpg",
                "LastUpdated": "2017-10-18T13:37:15:448+0000",
                "DescriptionShort": "Molly Hatch is an artist-of-all-trades who creates everything from furniture to jewelry to pen-and-ink drawings, but we especially love her idiosyncratic ceramics. With an MFA in the medium and several ceramic residences under her belt, her craft is finely honed, her designs whimsically literal and pop-culturally on-point.",
                "DescriptionLong": "Molly Hatch is an artist-of-all-trades who creates everything from furniture to jewelry to pen-and-ink drawings, but we especially love her idiosyncratic ceramics. With an MFA in the medium and several ceramic residences under her belt, her craft is finely honed, her designs whimsically literal and pop-culturally on-point.",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "QuantityText": "Expect availability",
                "Price": "36.00",
                "LocationID": "8c14428a-6382-4e8c-a88c-65c3bbf1b1e7",
                "Distance": 1.13,
                "ExternalProductId": "612180512",
                "RetLocationId": "19",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=7b6048e5-f610-476d-950e-b7cad62668a7&locationId=8c14428a-6382-4e8c-a88c-65c3bbf1b1e7&format=XML"
            },
            {
                "@attributes": {
                    "order": "3"
                },
                "ID": "1731d726-c526-44d0-b4bf-d8a9566140ea",
                "Name": "Anthropologie Tempered Glass iPhone Case",
                "MSRP": "16.00",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=680065296&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/bf/eb/bfeb4197ec6f99b7566a3e27df6199d0_medium/anthropologie-tempered-glass-iphone-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/bf/eb/bfeb4197ec6f99b7566a3e27df6199d0.jpg",
                "LastUpdated": "2017-10-18T13:37:15:448+0000",
                "DescriptionShort": "Fits iPhone 6/6s, 7, 8. Tempered glass. Imported",
                "DescriptionLong": "Fits iPhone 6/6s, 7, 8. Tempered glass. Imported",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "Size": "One Size",
                "QuantityText": "Expect availability",
                "Price": "16.00",
                "LocationID": "8c14428a-6382-4e8c-a88c-65c3bbf1b1e7",
                "Distance": 1.13,
                "ExternalProductId": "680065296",
                "RetLocationId": "19",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=1731d726-c526-44d0-b4bf-d8a9566140ea&locationId=8c14428a-6382-4e8c-a88c-65c3bbf1b1e7&format=XML"
            },
            {
                "@attributes": {
                    "order": "4"
                },
                "ID": "63b79a19-7cee-4f65-b3bd-c6b3f76c06a9",
                "Name": "Casetify Boho Elephant iPhone Case",
                "MSRP": "36.00",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=680065521&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/69/85/69855506e55795d2b52f248498756370_medium/casetify-boho-elephant-iphone-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/69/85/69855506e55795d2b52f248498756370.jpg",
                "LastUpdated": "2017-10-18T13:37:15:448+0000",
                "DescriptionShort": "The design mavens at Casetify create of-the-moment cases, watches and sleeves for all your modern gadgets. Crafted with a penchant for trend and color, each collection raises the standard for stylish accessorizing.",
                "DescriptionLong": "The design mavens at Casetify create of-the-moment cases, watches and sleeves for all your modern gadgets. Crafted with a penchant for trend and color, each collection raises the standard for stylish accessorizing.",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "QuantityText": "Expect availability",
                "Price": "36.00",
                "LocationID": "8c14428a-6382-4e8c-a88c-65c3bbf1b1e7",
                "Distance": 1.13,
                "ExternalProductId": "680065521",
                "RetLocationId": "19",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=63b79a19-7cee-4f65-b3bd-c6b3f76c06a9&locationId=8c14428a-6382-4e8c-a88c-65c3bbf1b1e7&format=XML"
            },
            {
                "@attributes": {
                    "order": "5"
                },
                "ID": "e93a380f-fa26-46b0-bbe3-e990ddf75293",
                "Name": "Wood'd The Future Is Female iPhone Case",
                "MSRP": "36.00",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=662318274&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/8c/50/8c50a690f7511855ed572f15bb63556c_medium/woodd-the-future-is-female-iphone-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/8c/50/8c50a690f7511855ed572f15bb63556c.jpg",
                "LastUpdated": "2017-10-18T13:37:15:448+0000",
                "DescriptionShort": "From their family's wood manufacturing factory, brothers Andrea and Stefano drew from their backgrounds in fashion and communication to create the first collection of Wood'd phone cases in 2012. Today, the Italian brand is recognized worldwide for its sleekly designed goods, crafted with high-quality materials and European refinement.",
                "DescriptionLong": "From their family's wood manufacturing factory, brothers Andrea and Stefano drew from their backgrounds in fashion and communication to create the first collection of Wood'd phone cases in 2012. Today, the Italian brand is recognized worldwide for its sleekly designed goods, crafted with high-quality materials and European refinement.",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "QuantityText": "Expect availability",
                "Price": "36.00",
                "LocationID": "8c14428a-6382-4e8c-a88c-65c3bbf1b1e7",
                "Distance": 1.13,
                "ExternalProductId": "662318274",
                "RetLocationId": "19",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=e93a380f-fa26-46b0-bbe3-e990ddf75293&locationId=8c14428a-6382-4e8c-a88c-65c3bbf1b1e7&format=XML"
            },
            {
                "@attributes": {
                    "order": "6"
                },
                "ID": "1d2106bd-2521-4c74-a667-377eaf224793",
                "Name": "Sonix Rabbit Iphone Case -",
                "MSRP": "35.00",
                "URL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4470899&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4470899%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5254191",
                "ImageSmall": "http://content.nordstrom.com/imagegallery/store/product/large/11/_13338811.jpg",
                "ImageLarge": "http://content.nordstrom.com/imagegallery/store/product/large/11/_13338811.jpg",
                "LastUpdated": "2017-03-01T11:46:04:858+0000",
                "DescriptionShort": "Shimmering rabbits and stars add a whimsical twist to a transparent hard-shell case that shows off your iPhone while protecting it from scuffs and scratches. Color(s): white. Brand: SONIX. Style Name: Sonix Rabbit Iphone Case (7 & 7 Plus). Style Number: 5254191. Available in stores.",
                "DescriptionLong": "Shimmering rabbits and stars add a whimsical twist to a transparent hard-shell case that shows off your iPhone while protecting it from scuffs and scratches. Color(s): white. Brand: SONIX. Style Name: Sonix Rabbit Iphone Case (7 & 7 Plus). Style Number: 5254191. Available in stores.",
                "SKU": "4470899",
                "Categories": {
                    "Primary": "Women",
                    "Secondary": "Small Leather Goods",
                    "Tertiary": "Cases & Covers"
                },
                "ProductCategory": "Apparel & Accessories",
                "QuantityText": "Expect availability",
                "Price": "35.00",
                "LocationID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Distance": 1.22,
                "ExternalProductId": "4470899",
                "RetLocationId": "1289421894",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=1d2106bd-2521-4c74-a667-377eaf224793&locationId=f3333995-ed7d-44e4-89d3-4626260ea6e1&format=XML",
                "ReserveURL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4470899&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4470899%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5254191"
            },
            {
                "@attributes": {
                    "order": "7"
                },
                "ID": "9f8785a7-5753-45df-9908-86bde6ddae5f",
                "Name": "Sonix Bisous Iphone Case -",
                "MSRP": "35.00",
                "URL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4469375&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4469375%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5252839",
                "ImageSmall": "http://content.nordstrom.com/imagegallery/store/product/large/10/_13144010.jpg",
                "ImageLarge": "http://content.nordstrom.com/imagegallery/store/product/large/10/_13144010.jpg",
                "LastUpdated": "2017-03-01T11:46:04:858+0000",
                "DescriptionShort": "Pretty red pouts give a little lip service to a stylish case designed with an impact-resistant rubber bumper to protect your favorite tech. Color(s): clear. Brand: SONIX. Style Name: Sonix Bisous Iphone Case (7 & 7 Plus). Style Number: 5252839. Available in stores.",
                "DescriptionLong": "Pretty red pouts give a little lip service to a stylish case designed with an impact-resistant rubber bumper to protect your favorite tech. Color(s): clear. Brand: SONIX. Style Name: Sonix Bisous Iphone Case (7 & 7 Plus). Style Number: 5252839. Available in stores.",
                "SKU": "4469375",
                "Categories": {
                    "Primary": "Women",
                    "Secondary": "Small Leather Goods",
                    "Tertiary": "Cases & Covers"
                },
                "ProductCategory": "Apparel & Accessories",
                "QuantityText": "Expect availability",
                "Price": "35.00",
                "LocationID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Distance": 1.22,
                "ExternalProductId": "4469375",
                "RetLocationId": "1289421894",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=9f8785a7-5753-45df-9908-86bde6ddae5f&locationId=f3333995-ed7d-44e4-89d3-4626260ea6e1&format=XML",
                "ReserveURL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4469375&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4469375%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5252839"
            },
            {
                "@attributes": {
                    "order": "8"
                },
                "ID": "77c22dc9-9b04-435e-bb2b-289f176c20d0",
                "Name": "Sonix Baillie Iphone Case -",
                "MSRP": "35.00",
                "URL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4471248&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4471248%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5254488",
                "ImageSmall": "http://content.nordstrom.com/imagegallery/store/product/large/11/_100006531.jpg",
                "ImageLarge": "http://content.nordstrom.com/imagegallery/store/product/large/11/_100006531.jpg",
                "LastUpdated": "2017-03-01T11:46:04:858+0000",
                "DescriptionShort": "Add some eye-catching style to your favorite tech with a slim, shock-absorbent case patterned in pink blossoms. Color(s): pink. Brand: SONIX. Style Name: Sonix Baillie Iphone Case (7 & 7 Plus). Style Number: 5254488. Available in stores.",
                "DescriptionLong": "Add some eye-catching style to your favorite tech with a slim, shock-absorbent case patterned in pink blossoms. Color(s): pink. Brand: SONIX. Style Name: Sonix Baillie Iphone Case (7 & 7 Plus). Style Number: 5254488. Available in stores.",
                "SKU": "4471248",
                "Categories": {
                    "Primary": "Women",
                    "Secondary": "Small Leather Goods",
                    "Tertiary": "Cases & Covers"
                },
                "ProductCategory": "Apparel & Accessories",
                "QuantityText": "Expect availability",
                "Price": "35.00",
                "LocationID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Distance": 1.22,
                "ExternalProductId": "4471248",
                "RetLocationId": "1289421894",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=77c22dc9-9b04-435e-bb2b-289f176c20d0&locationId=f3333995-ed7d-44e4-89d3-4626260ea6e1&format=XML",
                "ReserveURL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4471248&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4471248%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5254488"
            },
            {
                "@attributes": {
                    "order": "9"
                },
                "ID": "3b2b4b8d-99c6-4a43-bda1-50ce4f23a466",
                "Name": "Speck Clear Iphone Case -",
                "MSRP": "44.95",
                "URL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4488492&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4488492%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5267813",
                "ImageSmall": "http://content.nordstrom.com/imagegallery/store/product/large/0/_13178580.jpg",
                "ImageLarge": "http://content.nordstrom.com/imagegallery/store/product/large/0/_13178580.jpg",
                "LastUpdated": "2017-03-01T11:46:04:858+0000",
                "DescriptionShort": "An innovative, transparent case lets the sleek design of your iPhone take center stage. The patented, one-piece, multilayered construction is designed to absorb shock during occasional bumps and drops, while a raised bezel protects the screen and rubberized covers shield the buttons. Color(s): clear black matte. Brand: Speck. Style Name: Speck Clear Iphone Case (7/7 Plus). Style Number: 5267813. Available in stores.",
                "DescriptionLong": "An innovative, transparent case lets the sleek design of your iPhone take center stage. The patented, one-piece, multilayered construction is designed to absorb shock during occasional bumps and drops, while a raised bezel protects the screen and rubberized covers shield the buttons. Color(s): clear black matte. Brand: Speck. Style Name: Speck Clear Iphone Case (7/7 Plus). Style Number: 5267813. Available in stores.",
                "SKU": "4488492",
                "Categories": {
                    "Primary": "Women",
                    "Secondary": "Small Leather Goods",
                    "Tertiary": "Cases & Covers"
                },
                "ProductCategory": "Apparel & Accessories",
                "QuantityText": "Expect availability",
                "Price": "44.95",
                "LocationID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Distance": 1.22,
                "ExternalProductId": "4488492",
                "RetLocationId": "1289421894",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=3b2b4b8d-99c6-4a43-bda1-50ce4f23a466&locationId=f3333995-ed7d-44e4-89d3-4626260ea6e1&format=XML",
                "ReserveURL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4488492&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4488492%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5267813"
            },
            {
                "@attributes": {
                    "order": "10"
                },
                "ID": "aeac0527-0a7b-4434-a190-901d23a2de74",
                "Name": "Bandolier Cynthia Iphone Case -",
                "MSRP": "128.00",
                "URL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4586316&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4586316%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5336056",
                "ImageSmall": "http://content.nordstrom.com/imagegallery/store/product/large/16/_100430396.jpg",
                "ImageLarge": "http://content.nordstrom.com/imagegallery/store/product/large/16/_100430396.jpg",
                "LastUpdated": "2017-03-01T11:46:04:858+0000",
                "DescriptionShort": "Rummaging through your handbag to find your iPhone and missing that perfect shot? Not cool. A slim leather case that keeps your phone and cards super-handy while you're on the move? Brilliant. Color(s): grey/ pewter. Brand: BANDOLIER. Style Name: Bandolier Cynthia Iphone Case (7 & 7 Plus). Style Number: 5336056. Available in stores.",
                "DescriptionLong": "Rummaging through your handbag to find your iPhone and missing that perfect shot? Not cool. A slim leather case that keeps your phone and cards super-handy while you're on the move? Brilliant. Color(s): grey/ pewter. Brand: BANDOLIER. Style Name: Bandolier Cynthia Iphone Case (7 & 7 Plus). Style Number: 5336056. Available in stores.",
                "SKU": "4586316",
                "Categories": {
                    "Primary": "Women",
                    "Secondary": "Small Leather Goods",
                    "Tertiary": "Cases & Covers"
                },
                "ProductCategory": "Apparel & Accessories",
                "QuantityText": "Expect availability",
                "Price": "128.00",
                "LocationID": "f3333995-ed7d-44e4-89d3-4626260ea6e1",
                "Distance": 1.22,
                "ExternalProductId": "4586316",
                "RetLocationId": "1289421894",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=aeac0527-0a7b-4434-a190-901d23a2de74&locationId=f3333995-ed7d-44e4-89d3-4626260ea6e1&format=XML",
                "ReserveURL": "http://click.linksynergy.com/link?id=vfaIuldtxXc&offerid=276223.4586316&type=15&murl=http%3A%2F%2Fshop.nordstrom.com%2FS%2F4586316%3Fcm_mmc%3DLinkshare-_-datafeed-_-Women%3ASmallLeatherGoods%3ACases%26Covers-_-5336056"
            },
            {
                "@attributes": {
                    "order": "11"
                },
                "ID": "c8d51101-5fc3-4b5f-9d19-4dc2742f54d4",
                "Name": "Incase Pop Case for iPhone 6 and iPhone 6s",
                "URL": "http://www.apple.com/shop/product/HJLT2ZM/A/incase-pop-case-for-iphone-6-and-iphone-6s",
                "ImageSmall": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLV2/HJLV2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778199790",
                "ImageLarge": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLV2/HJLV2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778199790",
                "LastUpdated": "2016-03-31T14:46:38:770+0000",
                "DescriptionShort": "The Incase Pop Case for iPhone provides plenty of protection and minimalist style with a hardshell exterior and rubberized frame. Buy online now at apple.com.",
                "DescriptionLong": "The Incase Pop Case for iPhone provides plenty of protection and minimalist style with a hardshell exterior and rubberized frame. Buy online now at apple.com.",
                "SKU": "HJLT2ZM/A",
                "Categories": {
                    "Primary": "Iphone",
                    "Secondary": "Iphone Accessories",
                    "Tertiary": "Cases & Protection"
                },
                "ProductCategory": "Electronics",
                "Size": "4_7inch",
                "Color": "gray",
                "QuantityText": "Expect availability",
                "Price": "34.95",
                "LocationID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Distance": 1.29,
                "ExternalProductId": "375276247_hjlt2zma",
                "RetLocationId": "r146",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=c8d51101-5fc3-4b5f-9d19-4dc2742f54d4&locationId=c2b40948-d6e7-4cc3-b52b-da75b1c646be&format=XML"
            },
            {
                "@attributes": {
                    "order": "12"
                },
                "ID": "c4c7d373-e1e3-4984-9574-8228909115a4",
                "Name": "Sena Ultraslim Case for iPhone 6 and iPhone 6s",
                "URL": "http://www.apple.com/shop/product/HGZK2ZM/B/sena-ultraslim-case-for-iphone-6-plus-and-iphone-6s-plus",
                "ImageSmall": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/GZ/HGZK2/HGZK2?wid=572&hei=572&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1453546372378",
                "ImageLarge": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/GZ/HGZK2/HGZK2?wid=572&hei=572&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1453546372378",
                "LastUpdated": "2016-03-31T14:46:38:770+0000",
                "DescriptionShort": "The Sena UltraSlim Case for iPhone 6 Plus is hand-crafted from the finest quality pebble-textured leather. Buy now from the Apple Online Store.",
                "DescriptionLong": "The Sena UltraSlim Case for iPhone 6 Plus is hand-crafted from the finest quality pebble-textured leather. Buy now from the Apple Online Store.",
                "SKU": "HGZJ2ZM/B",
                "Categories": {
                    "Primary": "Iphone",
                    "Secondary": "Iphone Accessories",
                    "Tertiary": "Cases & Protection"
                },
                "ProductCategory": "Electronics",
                "Size": "4_7inch",
                "Color": "black",
                "QuantityText": "Expect availability",
                "Price": "39.95",
                "LocationID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Distance": 1.29,
                "ExternalProductId": "419028511_hgzj2zmb",
                "RetLocationId": "r146",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=c4c7d373-e1e3-4984-9574-8228909115a4&locationId=c2b40948-d6e7-4cc3-b52b-da75b1c646be&format=XML"
            },
            {
                "@attributes": {
                    "order": "13"
                },
                "ID": "9f33ebd2-ba85-4fe3-8705-e3a6df863eb0",
                "Name": "Incase Pop Case for iPhone 6 and iPhone 6s",
                "URL": "http://www.apple.com/shop/product/HJLT2ZM/A/incase-pop-case-for-iphone-6-and-iphone-6s",
                "ImageSmall": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLV2/HJLV2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778199790",
                "ImageLarge": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLV2/HJLV2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778199790",
                "LastUpdated": "2016-03-31T14:46:38:770+0000",
                "DescriptionShort": "The Incase Pop Case for iPhone provides plenty of protection and minimalist style with a hardshell exterior and rubberized frame. Buy online now at apple.com.",
                "DescriptionLong": "The Incase Pop Case for iPhone provides plenty of protection and minimalist style with a hardshell exterior and rubberized frame. Buy online now at apple.com.",
                "SKU": "HJLV2ZM/A",
                "Categories": {
                    "Primary": "Iphone",
                    "Secondary": "Iphone Accessories",
                    "Tertiary": "Cases & Protection"
                },
                "ProductCategory": "Electronics",
                "Size": "4_7inch",
                "Color": "midnight",
                "QuantityText": "Expect availability",
                "Price": "34.95",
                "LocationID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Distance": 1.29,
                "ExternalProductId": "375276247_hjlv2zma",
                "RetLocationId": "r146",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=9f33ebd2-ba85-4fe3-8705-e3a6df863eb0&locationId=c2b40948-d6e7-4cc3-b52b-da75b1c646be&format=XML"
            },
            {
                "@attributes": {
                    "order": "14"
                },
                "ID": "cd2680b2-ca3a-4e24-b2a3-6b40debaaf99",
                "Name": "Incase Pop Case for iPhone 6 and iPhone 6s",
                "URL": "http://www.apple.com/shop/product/HJLX2ZM/A/incase-pop-case-for-iphone-6-plus-and-iphone-6s-plus",
                "ImageSmall": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLY2/HJLY2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778182355",
                "ImageLarge": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/JL/HJLY2/HJLY2_SW_COLOR?wid=32&hei=32&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778182355",
                "LastUpdated": "2016-03-31T14:46:38:770+0000",
                "DescriptionShort": "The Incase Pop Case for iPhone 6 Plus provides plenty of hardshell protection and sleek minimalist style. Buy now from the Apple Online Store.",
                "DescriptionLong": "The Incase Pop Case for iPhone 6 Plus provides plenty of hardshell protection and sleek minimalist style. Buy now from the Apple Online Store.",
                "SKU": "HJLU2ZM/A",
                "Categories": {
                    "Primary": "Iphone",
                    "Secondary": "Iphone Accessories",
                    "Tertiary": "Cases & Protection"
                },
                "ProductCategory": "Electronics",
                "Size": "4_7inch",
                "Color": "black",
                "QuantityText": "Expect availability",
                "Price": "34.95",
                "LocationID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Distance": 1.29,
                "ExternalProductId": "953753945_hjlu2zma",
                "RetLocationId": "r146",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=cd2680b2-ca3a-4e24-b2a3-6b40debaaf99&locationId=c2b40948-d6e7-4cc3-b52b-da75b1c646be&format=XML"
            },
            {
                "@attributes": {
                    "order": "15"
                },
                "ID": "f613c710-64fd-450c-8c61-e021838c5729",
                "Name": "Sena Snap On Case for iPhone 6/iPhone 6s",
                "URL": "http://www.apple.com/shop/product/HHV92ZM/A/sena-snap-on-case-for-iphone-6-iphone-6s",
                "ImageSmall": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/HV/HHV92/HHV92?wid=572&hei=572&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778662355",
                "ImageLarge": "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/H/HV/HHV92/HHV92?wid=572&hei=572&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1454778662355",
                "LastUpdated": "2016-03-31T14:46:38:770+0000",
                "DescriptionShort": "The handcrafted leather Sena Snap On case for iPhone 6/6s beautifully combines sleek style and practicality. Buy online now at apple.com.",
                "DescriptionLong": "The handcrafted leather Sena Snap On case for iPhone 6/6s beautifully combines sleek style and practicality. Buy online now at apple.com.",
                "SKU": "HHV92ZM/A",
                "Categories": {
                    "Primary": "Iphone",
                    "Secondary": "Iphone Accessories",
                    "Tertiary": "Cases & Protection"
                },
                "ProductCategory": "Electronics",
                "QuantityText": "Expect availability",
                "Price": "59.95",
                "LocationID": "c2b40948-d6e7-4cc3-b52b-da75b1c646be",
                "Distance": 1.29,
                "ExternalProductId": "325578843_hhv92zma",
                "RetLocationId": "r146",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=f613c710-64fd-450c-8c61-e021838c5729&locationId=c2b40948-d6e7-4cc3-b52b-da75b1c646be&format=XML"
            },
            {
                "@attributes": {
                    "order": "16"
                },
                "ID": "85bba2ef-203d-4e7e-8750-d57bfa1f811d",
                "Name": "Pineapple iPhone 6 Case",
                "MSRP": "9.99",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=627804026&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/6e/29/6e29c915eaa1b56cff23d24b52cbb130_medium/charlotte-russe-pineapple-iphone-6-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/6e/29/6e29c915eaa1b56cff23d24b52cbb130.jpg",
                "LastUpdated": "2017-08-25T00:14:57:998+0000",
                "DescriptionShort": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "DescriptionLong": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "Size": "ONE",
                "QuantityText": "Expect availability",
                "Price": "9.99",
                "LocationID": "5ee0d386-660f-4779-b404-6ae186e28e93",
                "Distance": 1.3,
                "ExternalProductId": "627804026",
                "RetLocationId": "2633180919",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=85bba2ef-203d-4e7e-8750-d57bfa1f811d&locationId=5ee0d386-660f-4779-b404-6ae186e28e93&format=XML"
            },
            {
                "@attributes": {
                    "order": "17"
                },
                "ID": "6eb7bfa0-cae5-49e6-828d-5a1b19480767",
                "Name": "Pineapple iPhone 6 Case",
                "MSRP": "9.99",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=627804026&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/6e/29/6e29c915eaa1b56cff23d24b52cbb130_medium/charlotte-russe-pineapple-iphone-6-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/6e/29/6e29c915eaa1b56cff23d24b52cbb130.jpg",
                "LastUpdated": "2017-08-25T00:14:57:998+0000",
                "DescriptionShort": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "DescriptionLong": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "Size": "ONE",
                "QuantityText": "Expect availability",
                "Price": "9.99",
                "LocationID": "5ee0d386-660f-4779-b404-6ae186e28e93",
                "Distance": 1.3,
                "ExternalProductId": "627804026",
                "RetLocationId": "2633180919",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=6eb7bfa0-cae5-49e6-828d-5a1b19480767&locationId=5ee0d386-660f-4779-b404-6ae186e28e93&format=XML"
            },
            {
                "@attributes": {
                    "order": "18"
                },
                "ID": "284e42c9-ce03-4e74-8f8b-df8e4167ae29",
                "Name": "Corset iPhone 6 Case",
                "MSRP": "8.99",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=624848430&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/35/f2/35f27e6b6a430f38b422258eb085103a_medium/charlotte-russe-corset-iphone-6-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/35/f2/35f27e6b6a430f38b422258eb085103a.jpg",
                "LastUpdated": "2017-08-25T00:14:57:998+0000",
                "DescriptionShort": "Details content below Show your phone some love this Valentines season, with our super cute, corset case! Satin ribbons lace up the back into a dainty bow, with a cut-out heart camera opening on top! Product Fit: 2.75\" wide by 5.5\" tall Product Care: wipe clean / imported",
                "DescriptionLong": "Details content below Show your phone some love this Valentines season, with our super cute, corset case! Satin ribbons lace up the back into a dainty bow, with a cut-out heart camera opening on top! Product Fit: 2.75\" wide by 5.5\" tall Product Care: wipe clean / imported",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "Size": "ONE",
                "QuantityText": "Expect availability",
                "Price": "8.99",
                "LocationID": "5ee0d386-660f-4779-b404-6ae186e28e93",
                "Distance": 1.3,
                "ExternalProductId": "624848430",
                "RetLocationId": "2633180919",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?productId=284e42c9-ce03-4e74-8f8b-df8e4167ae29&locationId=5ee0d386-660f-4779-b404-6ae186e28e93&format=XML"
            },
            {
                "@attributes": {
                    "order": "19"
                },
                "ID": "b1794faf-aaa0-44ec-ad0d-13917f467801",
                "Name": "Pineapple iPhone 6 Case",
                "MSRP": "9.99",
                "URL": "http://api.shopstyle.com/action/apiVisitRetailer?id=627804026&pid=uid7041-26963929-59",
                "ImageSmall": "https://img.shopstyle-cdn.com/sim/6e/29/6e29c915eaa1b56cff23d24b52cbb130_medium/charlotte-russe-pineapple-iphone-6-case.jpg",
                "ImageLarge": "https://img.shopstyle-cdn.com/mim/6e/29/6e29c915eaa1b56cff23d24b52cbb130.jpg",
                "LastUpdated": "2017-08-25T00:14:57:998+0000",
                "DescriptionShort": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "DescriptionLong": "Details content below We love this rubber pineapple case for a tropical update to your phone! Compatible withiPhone 6 Product Fit: 2.75\" wide by 7.75\" tall Product Care: wipe clean / imported",
                "Categories": {
                    "Primary": "Tech Accessories"
                },
                "Size": "ONE",
                "QuantityText": "Expect availability",
                "Price": "9.99",
                "LocationID": "5ee0d386-660f-4779-b404-6ae186e28e93",
                "Distance": 1.3,
                "ExternalProductId": "627804026",
                "RetLocationId": "2633180919",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?&locationId=5ee0d386-660f-4779-b404-6ae186e28e93&format=XML"
            },
            {
                "@attributes": {
                    "order": "20"
                },
                "ID": "6ada172d-9dcb-4fc3-8fe8-881347d36d2e",
                "Name": "Play iPhone 6 Case",
                "LastUpdated": "2015-05-12T13:38:36:981+0000",
                "DescriptionShort": "Protect your phone in style with the Play iPhone 6 Case! A colorful print and the word \"play\" decorates the outside of this case. Pair with our additional phone & tech options for a complete look.",
                "DescriptionLong": "Protect your phone in style with the Play iPhone 6 Case! A colorful print and the word \"play\" decorates the outside of this case. Pair with our additional phone & tech options for a complete look.",
                "Categories": {
                    "Primary": "Home & Gifts",
                    "Secondary": "Phone & Tech"
                },
                "QuantityText": "Expect availability",
                "Price": "18.00",
                "LocationID": "4e10da9d-0693-421e-b171-c22c273b4c2e",
                "Distance": 1.3,
                "ExternalProductId": "3579",
                "RetLocationId": "47",
                "InventoryCheckURL": "http://api.REMOVED_URL.com/v2.0/inventory?locationId=4e10da9d-0693-421e-b171-c22c273b4c2e&format=XML"
            }
        ]
    }
}
```

## Search for "Bitcoin ultimate guide" with a radius of 100 miles

### Request:

```json
{
  "adid": "4c0f3c08-6903-408d-acb4-8aa14b48b1a3",
  "category": "0",
  "deviceType": "kinzie;android : 7.0",
  "geo_lat": 33.6321625,
  "geo_lon": -117.7348413,
  "keywords": "bitcoin ultimate guide",
  "location": "33.6321625,-117.7348413",
  "minprice": "0",
  "online": "0",
  "page_num": 1,
  "page_size": 20,
  "price": "0",
  "radius": "100",
  "requestorID": "9ea9976d8317735"
}
```

### Response:

```json
{
    "format": "json",
    "errorsExist": 0,
    "successMessage": "Item found",
    "results": {
        "retailers": [
            {
                "ID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Name": "Walmart",
                "PromotionalContent": null,
                "Logo": "http://images.REPLACED_URL.com/logos/walmart.png",
                "LogoSquare": "http://images.REPLACED_URL.com/logos/walmart-50x50.jpg",
                "Phs": "8"
            }
        ],
        "locations": [
            {
                "ID": "5621f31f-2929-4493-9d53-87baa9ca6b0f",
                "Name": "Walmart - Irvine",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "71 Technology Dr",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92618",
                    "Country": "US"
                },
                "Phone": "9492426587",
                "Latitude": "33.6569028",
                "Longitude": "-117.7417625",
                "MapLink": "http://maps.google.com/maps?q=71+Technology+Dr+Irvine+CA+92618",
                "Distance": "1.76",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=71+Technology+Dr+Irvine+CA+92618@33.6569028,-117.7417625&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "5687",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "98dbccc0-d039-4bea-a788-384808adb10d",
                "Name": "Walmart - Laguna Niguel",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "27470 Alicia Pkwy",
                    "City": "Laguna Niguel",
                    "State": "CA",
                    "ZIP": "92677",
                    "Country": "US"
                },
                "Phone": "9493600758",
                "Latitude": "33.5635107",
                "Longitude": "-117.7129607",
                "MapLink": "http://maps.google.com/maps?q=27470+Alicia+Pkwy+Laguna+Niguel+CA+92677",
                "Distance": "4.91",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=27470+Alicia+Pkwy+Laguna+Niguel+CA+92677@33.5635107,-117.7129607&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "2206",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "f788c66c-0956-4967-a53b-1e098d4727e3",
                "Name": "Walmart - Foothill Ranch",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "26502 Towne Centre Dr",
                    "City": "Foothill Ranch",
                    "State": "CA",
                    "ZIP": "92610",
                    "Country": "US"
                },
                "Phone": "9495887923",
                "Latitude": "33.6798507",
                "Longitude": "-117.6709609",
                "MapLink": "http://maps.google.com/maps?q=26502+Towne+Centre+Dr+Foothill+Ranch+CA+92610",
                "Distance": "4.94",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=26502+Towne+Centre+Dr+Foothill+Ranch+CA+92610@33.6798507,-117.6709609&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "2218",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "f49f8725-adce-467b-b2d5-22aac4689239",
                "Name": "Walmart - Irvine",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "16555 Von Karman Ave, Ste A",
                    "City": "Irvine",
                    "State": "CA",
                    "ZIP": "92606",
                    "Country": "US"
                },
                "Phone": "9496237467",
                "Latitude": "33.6989907",
                "Longitude": "-117.8362193",
                "MapLink": "http://maps.google.com/maps?q=16555+Von+Karman+Ave%2C+Ste+A+Irvine+CA+92606",
                "Distance": "7.44",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=16555+Von+Karman+Ave%2C+Ste+A+Irvine+CA+92606@33.6989907,-117.8362193&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "5644",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "9d1e1da3-26dc-4b12-8bdf-9ae016bd277f",
                "Name": "Walmart - Rancho Santa Margarita",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "30491 Avenida De Las Flores",
                    "City": "Rancho Santa Margarita",
                    "State": "CA",
                    "ZIP": "92688",
                    "Country": "US"
                },
                "Phone": "9492075070",
                "Latitude": "33.6407413",
                "Longitude": "-117.6014824",
                "MapLink": "http://maps.google.com/maps?q=30491+Avenida+De+Las+Flores+Rancho+Santa+Margarita+CA+92688",
                "Distance": "7.69",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=30491+Avenida+De+Las+Flores+Rancho+Santa+Margarita+CA+92688@33.6407413,-117.6014824&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "5600",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "9529c09a-be1a-4019-aed0-98ef095a8bfd",
                "Name": "Walmart - Santa Ana",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "3600 W Mcfadden Ave",
                    "City": "Santa Ana",
                    "State": "CA",
                    "ZIP": "92704",
                    "Country": "US"
                },
                "Phone": "7147751804",
                "Latitude": "33.7373373",
                "Longitude": "-117.9152328",
                "MapLink": "http://maps.google.com/maps?q=3600+W+Mcfadden+Ave+Santa+Ana+CA+92704",
                "Distance": "12.66",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=3600+W+Mcfadden+Ave+Santa+Ana+CA+92704@33.7373373,-117.9152328&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:00:00:23:59,2:00:00:23:59,3:00:00:23:59,4:00:00:23:59,5:00:00:23:59,6:00:00:23:59,7:00:00:23:59",
                "RetLocationId": "2517",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "52c3676b-c4cd-4484-8900-eaa760f55528",
                "Name": "Walmart - San Clemente",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "951 Avenida Pico",
                    "City": "San Clemente",
                    "State": "CA",
                    "ZIP": "92673",
                    "Country": "US"
                },
                "Phone": "9494986669",
                "Latitude": "33.4556448",
                "Longitude": "-117.6062412",
                "MapLink": "http://maps.google.com/maps?q=951+Avenida+Pico+San+Clemente+CA+92673",
                "Distance": "14.27",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=951+Avenida+Pico+San+Clemente+CA+92673@33.4556448,-117.6062412&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "2527",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "9b64b0f6-6923-43ad-bb52-31743f2f6596",
                "Name": "Walmart - Huntington Beach",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "21132 Beach Blvd",
                    "City": "Huntington Beach",
                    "State": "CA",
                    "ZIP": "92648",
                    "Country": "US"
                },
                "Phone": "7142744484",
                "Latitude": "33.6555494",
                "Longitude": "-117.987041",
                "MapLink": "http://maps.google.com/maps?q=21132+Beach+Blvd+Huntington+Beach+CA+92648",
                "Distance": "14.6",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=21132+Beach+Blvd+Huntington+Beach+CA+92648@33.6555494,-117.987041&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "5601",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "8d1232e7-2e22-4d72-bdc9-96e876a01801",
                "Name": "Walmart - Orange",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "2300 N Tustin St",
                    "City": "Orange",
                    "State": "CA",
                    "ZIP": "92865",
                    "Country": "US"
                },
                "Phone": "7149984473",
                "Latitude": "33.8281166",
                "Longitude": "-117.8385721",
                "MapLink": "http://maps.google.com/maps?q=2300+N+Tustin+St+Orange+CA+92865",
                "Distance": "14.79",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=2300+N+Tustin+St+Orange+CA+92865@33.8281166,-117.8385721&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "2546",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "6fb8398a-ea8a-4f17-a847-804702580d89",
                "Name": "Walmart - Huntington Beach",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "8230 Talbert Ave",
                    "City": "Huntington Beach",
                    "State": "CA",
                    "ZIP": "92646",
                    "Country": "US"
                },
                "Phone": "7148415390",
                "Latitude": "33.7002653",
                "Longitude": "-117.9855282",
                "MapLink": "http://maps.google.com/maps?q=8230+Talbert+Ave+Huntington+Beach+CA+92646",
                "Distance": "15.17",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=8230+Talbert+Ave+Huntington+Beach+CA+92646@33.7002653,-117.9855282&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:7:00:22:00,2:7:00:22:00,3:7:00:22:00,4:7:00:22:00,5:7:00:22:00,6:7:00:22:00,7:7:00:22:00",
                "RetLocationId": "2636",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "22a6a048-6dd6-4494-9133-0272455420c9",
                "Name": "Walmart - Anaheim",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "1120 S Anaheim Blvd",
                    "City": "Anaheim",
                    "State": "CA",
                    "ZIP": "92805",
                    "Country": "US"
                },
                "Phone": "6572085201",
                "Latitude": "33.8194798",
                "Longitude": "-117.9080622",
                "MapLink": "http://maps.google.com/maps?q=1120+S+Anaheim+Blvd+Anaheim+CA+92805",
                "Distance": "16.33",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=1120+S+Anaheim+Blvd+Anaheim+CA+92805@33.8194798,-117.9080622&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "5640",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "a0a06773-812f-4062-81b9-28ff10afc5ef",
                "Name": "Walmart - Garden Grove",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "10912 Katella Avenue",
                    "City": "Garden Grove",
                    "State": "CA",
                    "ZIP": "92840",
                    "Country": "US"
                },
                "Phone": "7146203822",
                "Latitude": "33.8010184",
                "Longitude": "-117.9423464",
                "MapLink": "http://maps.google.com/maps?q=10912+Katella+Avenue+Garden+Grove+CA+92840",
                "Distance": "16.68",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=10912+Katella+Avenue+Garden+Grove+CA+92840@33.8010184,-117.9423464&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "5639",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "ac8a02dc-ddd9-42f2-8c0b-f16d10fd51d7",
                "Name": "Walmart - Garden Grove",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "11822 Gilbert St",
                    "City": "Garden Grove",
                    "State": "CA",
                    "ZIP": "92841",
                    "Country": "US"
                },
                "Phone": "7145911300",
                "Latitude": "33.7907469",
                "Longitude": "-117.9642634",
                "MapLink": "http://maps.google.com/maps?q=11822+Gilbert+St+Garden+Grove+CA+92841",
                "Distance": "17.15",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=11822+Gilbert+St+Garden+Grove+CA+92841@33.7907469,-117.9642634&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "4171",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "d8cbc5b7-fb03-4fc5-a5a0-70fd42017e89",
                "Name": "Walmart - Westminster",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "13331 Beach Blvd",
                    "City": "Westminster",
                    "State": "CA",
                    "ZIP": "92683",
                    "Country": "US"
                },
                "Phone": "7147990020",
                "Latitude": "33.7678387",
                "Longitude": "-117.9930222",
                "MapLink": "http://maps.google.com/maps?q=13331+Beach+Blvd+Westminster+CA+92683",
                "Distance": "17.55",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=13331+Beach+Blvd+Westminster+CA+92683@33.7678387,-117.9930222&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "2495",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "c890b58f-c971-4bc2-a53d-663214fc2e9f",
                "Name": "Walmart - Stanton",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "12840 Beach Blvd",
                    "City": "Stanton",
                    "State": "CA",
                    "ZIP": "90680",
                    "Country": "US"
                },
                "Phone": "7142300126",
                "Latitude": "33.7749806",
                "Longitude": "-117.9911238",
                "MapLink": "http://maps.google.com/maps?q=12840+Beach+Blvd+Stanton+CA+90680",
                "Distance": "17.73",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=12840+Beach+Blvd+Stanton+CA+90680@33.7749806,-117.9911238&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "4134",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "86421a1a-74c2-473d-8952-f20176c82c41",
                "Name": "Walmart - Anaheim",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "440 N Euclid St",
                    "City": "Anaheim",
                    "State": "CA",
                    "ZIP": "92801",
                    "Country": "US"
                },
                "Phone": "7144910744",
                "Latitude": "33.8361695",
                "Longitude": "-117.937995",
                "MapLink": "http://maps.google.com/maps?q=440+N+Euclid+St+Anaheim+CA+92801",
                "Distance": "18.3",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=440+N+Euclid+St+Anaheim+CA+92801@33.8361695,-117.937995&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:00:00:23:59,2:00:00:23:59,3:00:00:23:59,4:00:00:23:59,5:00:00:23:59,6:00:00:23:59,7:00:00:23:59",
                "RetLocationId": "2242",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "4a14212b-cf5d-4809-aa99-01adb78cb72f",
                "Name": "Walmart - Corona",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "1290 E Ontario Ave",
                    "City": "Corona",
                    "State": "CA",
                    "ZIP": "92881",
                    "Country": "US"
                },
                "Phone": "9512780924",
                "Latitude": "33.846051",
                "Longitude": "-117.5411399",
                "MapLink": "http://maps.google.com/maps?q=1290+E+Ontario+Ave+Corona+CA+92881",
                "Distance": "18.5",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=1290+E+Ontario+Ave+Corona+CA+92881@33.846051,-117.5411399&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:00:00:23:59,2:00:00:23:59,3:00:00:23:59,4:00:00:23:59,5:00:00:23:59,6:00:00:23:59,7:00:00:23:59",
                "RetLocationId": "2842",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "a8ba41df-c163-4959-90cb-d1a6ba71038d",
                "Name": "Walmart - Corona",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "1560 West Sixth Street",
                    "City": "Corona",
                    "State": "CA",
                    "ZIP": "92882",
                    "Country": "US"
                },
                "Phone": "9513936405",
                "Latitude": "33.8770552",
                "Longitude": "-117.595742",
                "MapLink": "http://maps.google.com/maps?q=1560+West+Sixth+Street+Corona+CA+92882",
                "Distance": "18.71",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=1560+West+Sixth+Street+Corona+CA+92882@33.8770552,-117.595742&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:00:00,2:6:00:00:00,3:6:00:00:00,4:6:00:00:00,5:6:00:00:00,6:6:00:00:00,7:6:00:00:00",
                "RetLocationId": "2487",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "1b28dd5c-12ec-43f8-9358-a7218d46c467",
                "Name": "Walmart - Anaheim",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "88 E Orangethorpe Ave",
                    "City": "Anaheim",
                    "State": "CA",
                    "ZIP": "92801",
                    "Country": "US"
                },
                "Phone": "7144476751",
                "Latitude": "33.8575332",
                "Longitude": "-117.9188389",
                "MapLink": "http://maps.google.com/maps?q=88+E+Orangethorpe+Ave+Anaheim+CA+92801",
                "Distance": "18.82",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=88+E+Orangethorpe+Ave+Anaheim+CA+92801@33.8575332,-117.9188389&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:7:00:23:00,2:7:00:23:00,3:7:00:23:00,4:7:00:23:00,5:7:00:23:00,6:7:00:23:00,7:7:00:23:00",
                "RetLocationId": "5930",
                "TimeZone": "America/Los_Angeles"
            },
            {
                "ID": "d32f6117-3f52-4bb6-953c-9a55361c89a0",
                "Name": "Walmart - Anaheim",
                "RetailerID": "31d9b3fd-ea69-4e08-9f7d-f41bc1be1474",
                "Address": {
                    "Line1": "121 N Beach Blvd",
                    "City": "Anaheim",
                    "State": "CA",
                    "ZIP": "92801",
                    "Country": "US"
                },
                "Phone": "7148223179",
                "Latitude": "33.8332451",
                "Longitude": "-117.994978",
                "MapLink": "http://maps.google.com/maps?q=121+N+Beach+Blvd+Anaheim+CA+92801",
                "Distance": "20.41",
                "PromotionalContent": null,
                "TNavLink": "http://apps.scout.me/v1/driveto?dt=121+N+Beach+Blvd+Anaheim+CA+92801@33.8332451,-117.994978&token=s6iLlaxu-k6-J92rQBlaPvozpxUPAAPhbIlKgFy5X_XcyFLYv6iVIkXH4rOPIvZDR1duzTMYCzrFYAMkx2Q3RILIva9TGKubnreoRxaAv_2uRGGItiCo-FJA9sJA9EPtSO70OFeu7LDg06EZxiQjxU_CKNfi76c_-ImDVGNCocQ&name=Walmart",
                "Hours": "1:6:00:23:00,2:6:00:23:00,3:6:00:23:00,4:6:00:23:00,5:6:00:23:00,6:6:00:23:00,7:6:00:23:00",
                "RetLocationId": "3101",
                "TimeZone": "America/Los_Angeles"
            }
        ],
        "products": [
            {
                "@attributes": {
                    "order": "1"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "5621f31f-2929-4493-9d53-87baa9ca6b0f",
                "Distance": 1.76,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5687",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=5621f31f-2929-4493-9d53-87baa9ca6b0f&format=XML"
            },
            {
                "@attributes": {
                    "order": "2"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "98dbccc0-d039-4bea-a788-384808adb10d",
                "Distance": 4.91,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2206",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=98dbccc0-d039-4bea-a788-384808adb10d&format=XML"
            },
            {
                "@attributes": {
                    "order": "3"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "f788c66c-0956-4967-a53b-1e098d4727e3",
                "Distance": 4.94,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2218",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=f788c66c-0956-4967-a53b-1e098d4727e3&format=XML"
            },
            {
                "@attributes": {
                    "order": "4"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "f49f8725-adce-467b-b2d5-22aac4689239",
                "Distance": 7.44,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5644",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=f49f8725-adce-467b-b2d5-22aac4689239&format=XML"
            },
            {
                "@attributes": {
                    "order": "5"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "9d1e1da3-26dc-4b12-8bdf-9ae016bd277f",
                "Distance": 7.69,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5600",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=9d1e1da3-26dc-4b12-8bdf-9ae016bd277f&format=XML"
            },
            {
                "@attributes": {
                    "order": "6"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "9529c09a-be1a-4019-aed0-98ef095a8bfd",
                "Distance": 12.66,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2517",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=9529c09a-be1a-4019-aed0-98ef095a8bfd&format=XML"
            },
            {
                "@attributes": {
                    "order": "7"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "52c3676b-c4cd-4484-8900-eaa760f55528",
                "Distance": 14.27,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2527",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=52c3676b-c4cd-4484-8900-eaa760f55528&format=XML"
            },
            {
                "@attributes": {
                    "order": "8"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "9b64b0f6-6923-43ad-bb52-31743f2f6596",
                "Distance": 14.6,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5601",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=9b64b0f6-6923-43ad-bb52-31743f2f6596&format=XML"
            },
            {
                "@attributes": {
                    "order": "9"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "8d1232e7-2e22-4d72-bdc9-96e876a01801",
                "Distance": 14.79,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2546",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=8d1232e7-2e22-4d72-bdc9-96e876a01801&format=XML"
            },
            {
                "@attributes": {
                    "order": "10"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "6fb8398a-ea8a-4f17-a847-804702580d89",
                "Distance": 15.17,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2636",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=6fb8398a-ea8a-4f17-a847-804702580d89&format=XML"
            },
            {
                "@attributes": {
                    "order": "11"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "22a6a048-6dd6-4494-9133-0272455420c9",
                "Distance": 16.33,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5640",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=22a6a048-6dd6-4494-9133-0272455420c9&format=XML"
            },
            {
                "@attributes": {
                    "order": "12"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "a0a06773-812f-4062-81b9-28ff10afc5ef",
                "Distance": 16.68,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5639",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=a0a06773-812f-4062-81b9-28ff10afc5ef&format=XML"
            },
            {
                "@attributes": {
                    "order": "13"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "ac8a02dc-ddd9-42f2-8c0b-f16d10fd51d7",
                "Distance": 17.15,
                "ExternalProductId": "807897532432",
                "RetLocationId": "4171",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=ac8a02dc-ddd9-42f2-8c0b-f16d10fd51d7&format=XML"
            },
            {
                "@attributes": {
                    "order": "14"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "d8cbc5b7-fb03-4fc5-a5a0-70fd42017e89",
                "Distance": 17.55,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2495",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=d8cbc5b7-fb03-4fc5-a5a0-70fd42017e89&format=XML"
            },
            {
                "@attributes": {
                    "order": "15"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "c890b58f-c971-4bc2-a53d-663214fc2e9f",
                "Distance": 17.73,
                "ExternalProductId": "807897532432",
                "RetLocationId": "4134",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=c890b58f-c971-4bc2-a53d-663214fc2e9f&format=XML"
            },
            {
                "@attributes": {
                    "order": "16"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "86421a1a-74c2-473d-8952-f20176c82c41",
                "Distance": 18.3,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2242",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=86421a1a-74c2-473d-8952-f20176c82c41&format=XML"
            },
            {
                "@attributes": {
                    "order": "17"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "4a14212b-cf5d-4809-aa99-01adb78cb72f",
                "Distance": 18.5,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2842",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=4a14212b-cf5d-4809-aa99-01adb78cb72f&format=XML"
            },
            {
                "@attributes": {
                    "order": "18"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "a8ba41df-c163-4959-90cb-d1a6ba71038d",
                "Distance": 18.71,
                "ExternalProductId": "807897532432",
                "RetLocationId": "2487",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=a8ba41df-c163-4959-90cb-d1a6ba71038d&format=XML"
            },
            {
                "@attributes": {
                    "order": "19"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "1b28dd5c-12ec-43f8-9358-a7218d46c467",
                "Distance": 18.82,
                "ExternalProductId": "807897532432",
                "RetLocationId": "5930",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=1b28dd5c-12ec-43f8-9358-a7218d46c467&format=XML"
            },
            {
                "@attributes": {
                    "order": "20"
                },
                "ID": "264319a3-b150-417e-9fa4-38a0d4e07a4d",
                "Name": "The Ultimate Guide to Bitcoin",
                "URL": "http://www.walmart.com/ip/28825137",
                "ImageSmall": "http://i5.walmartimages.com/dfw/dce07b8c-5286/k2-_a8ae647a-6933-4293-84ce-867cce68afa8.v1.jpg",
                "ImageLarge": "http://i5.walmartimages.com/dfw/dce07b8c-1a96/k2-_e637e9e4-333c-4b52-8442-f2bbe2e306e7.v1.jpg",
                "LastUpdated": "2017-10-10T14:51:44:537+0000",
                "DescriptionShort": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed.",
                "DescriptionLong": "Bitcoin User Guide\" is the first user guide - written in layman's terms - to the global, anonymous electronic currency rapidly gaining widespread credibility. This cryptocurrency combats many problems with cash and credit transactions today. Bitcoins are resistant to inflation, they allow you to make purchases during travel if your credit account has been frozen by your bank due to \"suspicious activity,\" and protects from identity theft. Bitcoin is a peer-to-peer network of anonymous people, network protocols, and computer systems that has no authority over anybody's money. With the help of the \"Bitcoin User Guide,\" you will learn how to buy and sell a Bitcoin as well as how to make a purchase with Bitcoins. This book walks you through protecting your Bitcoins and mining Bitcoins. While most online tutorials are imcomplete or too technical, this book gives you the basic information you need and can understand to this new electronic currency. This book gives you pros and cons to using Bitcoins. As a newcomer to the technology, this book will teach you how to know which mobile wallet is best for you. Whether you're using a desktop, laptop, or mobile device, you'll learn the technology, no prior knowledge assumed..",
                "SKU": "28825137",
                "Categories": {
                    "Primary": "Books",
                    "Secondary": "Business & Investing",
                    "Tertiary": "General"
                },
                "ProductCategory": "Media",
                "QuantityText": "Expect availability",
                "Price": "18.44",
                "LocationID": "d32f6117-3f52-4bb6-953c-9a55361c89a0",
                "Distance": 20.41,
                "ExternalProductId": "807897532432",
                "RetLocationId": "3101",
                "InventoryCheckURL": "http://api.REPLACED_URL.com/v2.0/inventory?productId=264319a3-b150-417e-9fa4-38a0d4e07a4d&locationId=d32f6117-3f52-4bb6-953c-9a55361c89a0&format=XML"
            }
        ]
    }
}
```
